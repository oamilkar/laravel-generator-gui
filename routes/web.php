<?php

Route::redirect('builder', 'generator-builder');
Route::redirect('generator', 'generator-builder');
Route::redirect('generator_builder', 'generator-builder');
Route::prefix('generator-builder')->name('generator-builder::')->group(function()
{
    // main view displayed to the user
    Route::get('', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')
        ->name('io_generator_builder');

    // main "generate" method
    Route::post('generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')
        ->name('generate');



    // **************************
    // vue components
    // **************************
    Route::get('fields.js', function(){
        return response()->vue('fields');
    })->name('fields.js');

    Route::get('relations.js', function(){
        return response()->vue('relations');
    })->name('relations.js');

    Route::get('sample.js', function(){
        return response()->vue('sample');
    })->name('sample.js');

    // **************************
    // end: vue components
    // **************************

});



Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')
    ->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')
    ->name('io_relation_field_template');


Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')
    ->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');