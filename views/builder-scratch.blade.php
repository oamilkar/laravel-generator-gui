@verbatim

<!-- -------------------------------------- -->
<!-- MODEL + GENERATOR OPTIONS -->
<!-- -------------------------------------- -->
<div class="card-body">
    <!-- -------------------------------------- -->
    <!-- MODEL OPTIONS -->
    <!-- -------------------------------------- -->
    <div class="row">
        <div class="form-group col-md-4">
            <label for="txtModelName">Model Name<span class="required">*</span></label>
            <input type="text" class="form-control" required id="txtModelName" placeholder="Enter name"
                v-model="modelName">
        </div>
        <div class="form-group col-md-4">
            <label for="txtCustomTblName">Custom Table Name</label>
            <input type="text" class="form-control" id="txtCustomTblName" placeholder="Enter table name"
                v-model="tableName">
        </div>

        <div class="form-group col-md-3">
            <label for="txtPrefix">Prefix</label>
            <input type="text" class="form-control" id="txtPrefix" placeholder="Enter prefix"
                v-model="options.prefix">
        </div>

        <div class="form-group col-md-1">
            <label for="txtPaginate">Paginate</label>
            <input type="number" class="form-control" id="txtPaginate" placeholder="default"
                v-model="options.paginate">
        </div>
    </div>

    <div class="row">
        <!-- -------------------------------------- -->
        <!-- CONTROLLER OPTIONS -->
        <!-- -------------------------------------- -->
        <div class="col col-md-2" >
            <div class="card">
                <div class="card-header pb-1 bg-light" >
                    <h3 class="card-title">Controllers</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" id="controller-web" v-model="controllers.web" >
                                <label for="controller-web">WEB</label>
                            </div>
                        </div>
                        <div class="col col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" id="api" v-model="controllers.api" >
                                <label for="api">API</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- -------------------------------------- -->
        <!-- PATTERN OPTIONS -->
        <!-- -------------------------------------- -->
        <div class="col col-md-6" >
            <div class="card">
                <div class="card-header pb-1 bg-light" >
                    <h3 class="card-title">Pattern Options</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="repositoryPattern" v-model="options.repositoryPattern" >
                                <label for="repositoryPattern">Repository</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="localized" v-model="options.localized" >
                                <label for="localized">Localized</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="datatables" v-model="addOns.datatables" >
                                <label for="datatables">Datatables</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="resources" v-model="options.resources" >
                                <label for="resources">Resource</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="swagger" v-model="options.swagger" >
                                <label for="swagger">Swagger</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="tests" v-model="options.tests" >
                                <label for="tests">Tests</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- -------------------------------------- -->
        <!-- MODEL OPTIONS -->
        <!-- -------------------------------------- -->
        <div class="col" >
            <div class="card">
                <div class="card-header pb-1 bg-light" >
                    <h3 class="card-title">Model Options</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <!-- <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="migrate" v-model="migrate" >
                                <label for="migrate">Migration</label>
                            </div>
                        </div> -->
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="factory" v-model="options.factory" >
                                <label for="factory">Factory</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="softDelete" v-model="options.softDelete" >
                                <label for="softDelete">Soft&nbsp;Delete</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="seeder" v-model="options.seeder" >
                                <label for="seeder">Seeder</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


<!-- -------------------------------------- -->
<!-- SECTION: FIELDS -->
<!-- -------------------------------------- -->
<div class="card-header pb-1 bg-light" >
    <h3 class="card-title" style="font-size: 1.5em;"><i class="far fa-edit"></i> Fields</h3>
    <span class="badge bg-info float-left ml-1">{{ fields.length }}</span>
    <div class="card-tools my-0">
        <button type="button" class="btn btn-tool" @click="addPrimary()">
            <i class="fa fa-key"></i> Add Primary
        </button>
        <button type="button" class="btn btn-tool" @click="addField()">
            <i class="fa fa-plus"></i> Add Field
        </button>
        <button type="button" class="btn btn-tool" @click="addTimestamps()">
            <i class="far fa-clock"></i> Add Timestamps
        </button>
    </div>
</div>
<div class="card-body pt-2">
    <div class="table-responsive">
        <fields-table ref="fields_table" v-model="fields" />
    </div>
</div>


<!-- -------------------------------------- -->
<!-- SECTION: RELATIONS -->
<!-- -------------------------------------- -->
<div class="card-header pb-1 bg-light" >
    <h3 class="card-title" style="font-size: 1.5em;"><i class="fas fa-sitemap"></i> Relations</h3>
    <span class="badge bg-info float-left ml-1">{{ relations.length }}</span>
    <div class="card-tools my-0">
        <button type="button" class="btn btn-tool" @click="$refs.relations_table.addRelation()" >
            <i class="fa fa-plus"></i> Add RelationShip
        </button>
    </div>
</div>
<div class="card-body pt-2">
    <div class="table-responsive" id="relationShip" >
        <relations-table ref="relations_table" v-model="relations" :fields="fields" />
    </div>
</div>


<!-- -------------------------------------- -->
<!-- SECTION: FOOTER -->
<!-- -------------------------------------- -->
<div class="card-footer mt-1">
    <div class="form-inline">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <input type="checkbox" id="saveSchemaFile" v-model="options.saveSchemaFile" >
                    <label for="saveSchemaFile" class="ml-1">Save&nbsp;Schema</label>
                </div>
                <!-- <div class="input-group-text">
                    <input type="checkbox" id="forceMigrate" v-model="options.forceMigrate" >
                    <label for="forceMigrate" class="ml-1">Run migration</label>
                </div> -->
            </div>
            <div class="input-group-append">
                <button type="button" class="btn btn-primary" id="btnGenerate" @click="submit()">
                    Generate
                </button>
            </div>
        </div>
    </div>
</div>


<!-- -------------------------------------- -->
<!-- MODAL: RESET CONFIRMATION -->
<!-- -------------------------------------- -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Reset</h4>
            </div>

            <div class="modal-body">
                <p style="font-size: 16px">
                    This will reset all of your fields. Do you want to proceed?
                </p>
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">
                    No
                </button>
                <button type="button" class="btn btn-flat btn-danger btn-ok" data-dismiss="modal"
                    @click="clear()">Yes
                </button>
            </div>
        </div>
    </div>
</div>
@endverbatim


@push('vuejs')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script type="module">
        import { createApp, ref } from 'vue'
        import fields from  '{{ route("generator-builder::fields.js") }}?v={{ time() }}'
        import relations from  '{{ route("generator-builder::relations.js") }}?v={{ time() }}'
        import sample from '{{ route("generator-builder::sample.js") }}?v={{ time() }}'

        const app = createApp({
            data() {
                return {
                    modelName: '',
                    tableName: '',

                    migrate: true,
                    controllers: {
                        web: true,
                        api: true,
                    },
                    options: {
                        prefix: null,
                        paginate: null,
                        
                        repositoryPattern: null,
                        localized: true,

                        saveSchemaFile: true,
                        resources: null,
                        swagger: false,
                        
                        factory: true,
                        seeder: true,
                        softDelete: false,
                        tests: false,

                        forceMigrate: false,
                    },
                    addOns: {
                        datatables: true,
                    },

                    fields: [],
                    relations: [],
                }
            },
            mounted() {
                $('#app').removeClass('invisible');
            },
            methods: {
                submit(){
                    let url = '{{ route("generator-builder::generate") }}';
                    let _token = '{{ csrf_token() }}';

                    let controllers = this.controllers;
                    let commandType = controllers.api ? 'infyom:api' : 'infyom:scaffold';
                    if(controllers.api && controllers.web) commandType += '_scaffold';

                    let {modelName, tableName, migrate, options, addOns, fields, relations } = this;
                    let data = {
                        modelName, tableName, migrate, options, addOns, fields, relations,
                        commandType, _token
                    };

                    axios.post(url, data).then(response => {
                        console.log('got response', response);
                        toastr.success(response.data);

                        this.clear();
                    }).catch(error => {
                        console.log('got error', error);
                        let response = error.response;
                        toastr.error(response.data.message);
                    });
                },
                clear(){
                    this.modelName = '';
                    this.tableName = '';
                    this.fields.splice(0, this.fields.length);
                    this.relations.splice(0, this.relations.length);

                    this.$refs.fields_table.addDefaults();
                },
                onApi(){
                    if (this.controllers.web) {
                        this.repositoryPattern = true;
                    }                    
                },
                onWeb(){
                    if (this.controllers.api) {
                        this.repositoryPattern = true;
                    }
                },
                addPrimary(){
                    this.$refs.fields_table.addPrimary();
                },
                addField(){
                    this.$refs.fields_table.addField();
                },
                addTimestamps(){
                    this.$refs.fields_table.addTimestamps();
                },
            },
            computed:{
                hasTimestamps(){
                    // check if fields contains created_at and updated_at
                    const search = ['created_at', 'updated_at'];
                    return search.every(v => this.fields.some(f => f.name == v));
                },
            },
        });

        app
        .component('sample', sample)
        .component('fields-table', fields)
        .component('relations-table', relations)
        .mount('#app');
    </script>
@endpush