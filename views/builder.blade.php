<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CRUD Generator GUI</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"> -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.2/dist/css/adminlte.min.css">


    <style>
        .required {
            color: red;
            padding-left: 5px;
        }
        .table td{
            padding-top: 0.25rem;
            padding-bottom: 0.25rem;
        }
        .table th{
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

    </style>
</head>

<body class="skin-blue" style="background-color: #ecf0f5">
    <div id="app" class="content mt-3 invisible">
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-lg-11 col-md-12">
                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="tab-tab" role="tablist">
                                <li class="pt-2 px-3">
                                    <h3 class="card-title">CRUD Generator</h3>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab-scratch-tab" data-toggle="pill" href="#tab-scratch"
                                        role="tab" aria-controls="tab-scratch" aria-selected="true">From scratch</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab-schema-tab" data-toggle="pill" href="#tab-schema"
                                        role="tab" aria-controls="tab-schema" aria-selected="false">From Schema</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab-table-tab" data-toggle="pill" href="#tab-table" disabled
                                        role="tab" aria-controls="tab-table" aria-selected="false">From Table</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab-rollback-tab" data-toggle="pill" href="#tab-rollback" disabled
                                        role="tab" aria-controls="tab-rollback" aria-selected="false">Rollback</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" id="tab-tabContent">
                            <!-- TAB: from-scratch -->
                            <div class="tab-pane fade show active" id="tab-scratch" role="tabpanel" aria-labelledby="tab-scratch-tab">
                                @include('generator-builder::builder-scratch')
                            </div>

                            <!-- TAB: from-schema -->
                            <div class="tab-pane fade" id="tab-schema" role="tabpanel" aria-labelledby="tab-schema-tab">
                                <div class="card-body">
                                    <sample></sample>
                                </div>
                            </div>

                            <!-- TAB: from-table -->
                            <div class="tab-pane fade" id="tab-table" role="tabpanel" aria-labelledby="tab-table-tab">

                            </div>

                            <!-- TAB: rollback -->
                            <div class="tab-pane fade" id="tab-rollback" role="tabpanel" aria-labelledby="tab-rollback-tab">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="importmap">
    {
        "imports": {
            "vue": "https://unpkg.com/vue@3/dist/vue.esm-browser.js"
        }
    }
    </script>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    @stack('vuejs')
</body>

</html>