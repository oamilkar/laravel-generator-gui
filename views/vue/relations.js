import { ref } from 'vue'

export default {
  props: ['modelValue', 'fields'],
  emits: ['update:modelValue'],

  data() {
    return {
      count: 0,
    }
  },

  mounted(){
    $('th').tooltip({ boundary: 'window', placement:'top' });
  },
  methods: {
    addRelation(field){
      this.modelValue.push( field || {
        relType:'1t1',
      })
    },
    removeRelation(item) {
      let i = this.modelValue.indexOf(item);
      if(i >= 0) this.modelValue.splice(i, 1);
    }
  },

  /*html*/
  template: `{!! $template !!}`
}