import { ref, nextTick } from 'vue'

export default {
  props: ['modelValue'],
  emits: ['update:modelValue'],

  data() {
    return {
      count: 0,
    }
  },

  mounted(){
    $('th').tooltip({ boundary: 'window', placement:'top' });
    this.addDefaults();
  },
  updated(){
    // $('td').tooltip({ boundary: 'window', placement:'top' });
  },
  methods: {
    addDefaults(){
      this.addPrimary();
      this.addField({
        name:'name', dbType:'string', htmlType:'text', validations:'required',
        inIndex:true, searchable: true, fillable:true, inForm: true});
    },
    addPrimary(){
      this.addField({
        name:'id',
        dbType:'increments',
        htmlType: 'text',
        primary: true,
        searchable: false,
        inView: false,
        inForm: false,
        fillable: false,
      });
    },
    addTimestamps(){
      const timestamps = ['created_at', 'updated_at'];
      timestamps.forEach((name) => {
        if(!this.modelValue.find((item) => item.name == name))
        {
          this.addField({
            name:name, dbType:'timestamp', htmlType:'datetime', 
            inIndex:true, inForm:false, fillable:false
          });
        }
      });
    },
    addField(atts){
      let field = Object.assign(this.getDefaultField(), atts || {});
      this.modelValue.push(field)
    },
    getDefaultField(){
      return {
        dbType:'string',
        htmlType:'text',
        primary: false,
        isForeign: false,
        inIndex: true,
        searchable: true,
        inView: true,
        inForm: true,
        fillable: true,
      };
    },
    removeField(item) {
      let i = this.modelValue.indexOf(item);
      if(i >= 0) this.modelValue.splice(i, 1);
    },
    focus(e, i){
      if(i < 0 || i > this.modelValue.length) return;

      const sel_tokens = [e.target.localName, ...e.target.classList];
      const selector = sel_tokens.join('.');
      const inputElement = this.$el.querySelectorAll(selector)[i];

      if (inputElement) {
        inputElement.focus();
        if(inputElement.type == 'text') {
          inputElement.setSelectionRange(0,0);
        }
      }
    },

  },
  computed: {
    indexFields(){
      const items = this.modelValue.filter((item) => item.inIndex || false)
      return items;
    }
  },

  /*html*/
  template: `{!! $template !!}`
}