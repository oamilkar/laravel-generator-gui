import { ref } from 'vue'

export default {
  setup() {
    const count = ref(0)
    return { count }
  },
  /*html*/
  template: `<div>count is {{ count }}</div>`
}