<div class="col-lg-10 col-lg-offset-1 col-md-12">
        <section class="content">
            <div id="info" style="display: none"></div>

            <form id="form">
                <div class="box box-primary col-lg-12">
                    <div class="box-header with-border" style="margin-top: 10px">
                        <h1 class="box-title" style="font-size: 30px">Laravel CRUD Generator</h1>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="_token" id="token" value="{!! csrf_token() !!}" />

                        <div class="form-group col-md-4">
                            <label for="txtModelName">Model Name<span class="required">*</span></label>
                            <input type="text" class="form-control" required id="txtModelName" placeholder="Enter name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtCustomTblName">Custom Table Name</label>
                            <input type="text" class="form-control" id="txtCustomTblName" placeholder="Enter table name">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="txtPrefix">Prefix</label>
                            <input type="text" class="form-control" id="txtPrefix" placeholder="Enter prefix">
                        </div>

                        <div class="form-group col-md-1">
                            <label for="txtPaginate">Paginate</label>
                            <input type="number" class="form-control" value="10" id="txtPaginate" placeholder="">
                        </div>

                        <!-- -------------------------------------- -->
                        <!-- OPTIONS -->
                        <!-- -------------------------------------- -->
                        <div class="form-group col-md-9">
                            <label for="txtModelName">Options</label>

                            <div class="form-inline form-group" style="border-color: transparent">
                                <div class="checkbox chk-align">
                                    <label>
                                        <input type="checkbox" class="flat-red" id="chkSave" checked> <span class="chk-label-margin">Save Schema</span>
                                    </label>
                                </div>
                                <div class="checkbox chk-align">
                                    <label>
                                        <input type="checkbox" class="flat-red" id="chkDelete" checked><span class="chk-label-margin"> Soft Delete </span>
                                    </label>
                                </div>
                                <div class="checkbox chk-align" id="chDataTable">
                                    <label>
                                        <input type="checkbox" class="flat-red" id="chkDataTable" checked> <span class="chk-label-margin">Datatables</span>
                                    </label>
                                </div>
                                <div class="checkbox chk-align" id="chMigration">
                                    <label>
                                        <input type="checkbox" class="flat-red" id="chkMigration" checked> <span class="chk-label-margin">Migration</span>
                                    </label>
                                </div>
                                <div class="checkbox chk-align" id="chForceMigrate">
                                    <label>
                                        <input type="checkbox" class="flat-red" id="chkForceMigrate"> <span class="chk-label-margin">Force Migrate</span>
                                    </label>
                                </div>
                                <div class="checkbox chk-align" id="chSwag">
                                    <label>
                                        <input type="checkbox" class="flat-red" id="chkSwagger"> <span class="chk-label-margin">Swagger</span>
                                    </label>
                                </div>
                                <div class="checkbox chk-align" id="chTest">
                                    <label>
                                        <input type="checkbox" class="flat-red" id="chkTestCases"> <span class="chk-label-margin">Test Cases</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- -------------------------------------- -->
                    <!-- FIELDS: HEADER -->
                    <!-- -------------------------------------- -->
                    <div class="box-header with-border" style="margin-top: 10px">
                        <h2 class="box-title" style="font-size: 28px">Fields</h2>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-primary btn-flat" id="btnPrimary">
                                <i class="fa fa-key"></i> Add Primary
                            </button>
                            <button type="button" class="btn btn-primary btn-flat" id="btnAdd">
                                <i class="fa fa-plus"></i> Add Field
                            </button>
                            <button type="button" class="btn btn-primary btn-flat" id="btnTimeStamps">
                                <i class="fa fa-clock-o"></i> Add Timestamps
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <!-- -------------------------------------- -->
                        <!-- FIELDS: TABLE -->
                        <!-- -------------------------------------- -->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="table">
                                <thead class="no-border">
                                    <tr>
                                        <th>Field Name</th>
                                        <th>DB Type</th>
                                        <th>Validations</th>
                                        <th>Html Type</th>
                                        <th style="width: 68px">Primary</th>
                                        <th style="width: 80px">Is Foreign</th>
                                        <th style="width: 87px">Searchable</th>
                                        <th style="width: 63px">Fillable</th>
                                        <th style="width: 65px">In Form</th>
                                        <th style="width: 67px">In Index</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="container" class="no-border-x no-border-y ui-sortable">

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <!-- -------------------------------------- -->
                    <!-- RELATIONS: HEADER -->
                    <!-- -------------------------------------- -->
                    <div class="box-header with-border" style="margin-top: 10px">
                        <h2 class="box-title" style="font-size: 28px">Relations</h2>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-primary btn-flat" id="btnRelationShip">
                                <i class="fa fa-code-fork"></i> Add RelationShip
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <!-- -------------------------------------- -->
                        <!-- RELATIONS -->
                        <!-- -------------------------------------- -->
                        <div class="table-responsive" id="relationShip" style="margin-top:55px;display: none">
                            <table class="table table-striped table-bordered" id="table">
                                <thead class="no-border">
                                    <tr>
                                        <th>Relation Type</th>
                                        <th>Foreign Model<span class="required">*</span></th>
                                        <th>Foreign Key</th>
                                        <th>Local Key</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="rsContainer" class="no-border-x no-border-y ui-sortable">

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="box-footer">
                        <!-- -------------------------------------- -->
                        <!-- FORM FOOTER -->
                        <!-- -------------------------------------- -->
                        <div class="form-inline col-md-12" style="padding:15px 15px;text-align: right">
                            <div class="form-group" style="text-align: left">
                                <label for="drdCommandType">Command Type</label>
                                <select id="drdCommandType" class="form-control" style="width: 100%">
                                    <option value="infyom:api_scaffold">API Scaffold Generator</option>
                                    <option value="infyom:api">API Generator</option>
                                    <option value="infyom:scaffold">Scaffold Generator</option>
                                </select>
                            </div>

                            <div class="form-group" style="border-color: transparent;padding-left: 10px">
                                <label style="display:block">&nbsp;</label>
                                <button type="submit" class="btn btn-flat btn-success" id="btnGenerate">
                                    Generate
                                </button>
                            </div>
                            <div class="form-group" style="border-color: transparent;padding-left: 10px">
                                <label style="display:block">&nbsp;</label>
                                <button type="button" class="btn btn-default btn-flat" id="btnReset" data-toggle="modal" data-target="#confirm-delete">
                                    Reset
                                </button>
                            </div>
                        </div>

                        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Confirm Reset</h4>
                                    </div>

                                    <div class="modal-body">
                                        <p style="font-size: 16px">This will reset all of your fields. Do you want to
                                            proceed?</p>

                                        <p class="debug-url"></p>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">No
                                        </button>
                                        <a id="btnModelReset" class="btn btn-flat btn-danger btn-ok" data-dismiss="modal">Yes</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>

        </section>
    </div>



    <div class="col-lg-10 col-lg-offset-1 col-md-12">
        <section class="content">
            <div id="rollbackInfo" style="display: none"></div>
            <div class="box box-primary col-lg-12">
                <div class="box-header" style="margin-top: 10px">
                    <h1 class="box-title" style="font-size: 30px">Rollback</h1>
                </div>
                <div class="box-body">
                    <form id="rollbackForm">
                        <input type="hidden" name="_token" id="rbToken" value="{!! csrf_token() !!}" />

                        <div class="form-group col-md-4">
                            <label for="txtRBModelName">Model Name<span class="required">*</span></label>
                            <input type="text" class="form-control" required id="txtRBModelName" placeholder="Enter name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="drdRBCommandType">Command Type</label>
                            <select id="drdRBCommandType" class="form-control" style="width: 100%">
                                <option value="api_scaffold">API Scaffold Generator</option>
                                <option value="api">API Generator</option>
                                <option value="scaffold">Scaffold Generator</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtRBPrefix">Prefix</label>
                            <input type="text" class="form-control" id="txtRBPrefix" placeholder="Enter prefix">
                        </div>
                        <div class="form-inline col-md-12" style="padding:15px 15px;text-align: right">
                            <div class="form-group" style="border-color: transparent;padding-left: 10px">
                                <button type="submit" class="btn btn-flat btn-primary btn-blue" id="btnRollback">Rollback
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-10 col-lg-offset-1 col-md-12">
        <section class="content">
            <div id="schemaInfo" style="display: none"></div>
            <div class="box box-primary col-lg-12">
                <div class="box-header" style="margin-top: 10px">
                    <h1 class="box-title" style="font-size: 30px">Generate CRUD From Schema</h1>
                </div>
                <div class="box-body">
                    <form method="post" id="schemaForm" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="smToken" value="{!! csrf_token() !!}" />
                        <div class="form-group col-md-4">
                            <label for="txtSmModelName">Model Name<span class="required">*</span></label>
                            <input type="text" name="modelName" class="form-control" id="txtSmModelName" placeholder="Enter Model Name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="schemaFile">Schema File<span class="required">*</span></label>
                            <input type="file" name="schemaFile" class="form-control" required id="schemaFile">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="drdSmCommandType">Command Type</label>
                            <select name="commandType" id="drdSmCommandType" class="form-control" style="width: 100%">
                                <option value="infyom:api_scaffold">API Scaffold Generator</option>
                                <option value="infyom:api">API Generator</option>
                                <option value="infyom:scaffold">Scaffold Generator</option>
                            </select>
                        </div>
                        <div class="form-inline col-md-12" style="padding:15px 15px;text-align: right">
                            <div class="form-group" style="border-color: transparent;padding-left: 10px">
                                <button type="submit" class="btn btn-flat btn-primary btn-blue" id="btnSmGenerate">Generate
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>