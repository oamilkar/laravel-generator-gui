<?php

namespace InfyOm\GeneratorBuilder;

use Illuminate\Support\ServiceProvider;
use InfyOm\GeneratorBuilder\Commands\GeneratorBuilderRoutesPublisherCommand;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;
use Illuminate\Support\Facades\Blade;


class GeneratorBuilderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__.'/../config/generator_builder.php';


        $this->publishes([
            $configPath => config_path('infyom/generator_builder.php'),
        ]);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__.'/../views/', 'generator-builder');

        Response::macro('js', function (string $value) {
            return $this->file($value, ['Content-Type' => 'application/javascript']);
        });
        Response::macro('vue', function (string $value) {
            $html = file_get_contents(__DIR__."/../views/vue/{$value}.html");
            $js = file_get_contents(__DIR__."/../views/vue/{$value}.js");
            $content = Blade::render($js , ['template' => $html]);

            return response($content)->header('Content-Type', 'application/javascript');
        });

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('infyom.publish.generator-builder', function ($app) {
            return new GeneratorBuilderRoutesPublisherCommand();
        });

        $this->commands([
            'infyom.publish.generator-builder',
        ]);
    }
}
